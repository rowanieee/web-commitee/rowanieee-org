const express = require('express');
const path = require('path');
const { env } = require('process');
const bodyParser = require('body-parser');

const routes = {
  tutoring: require('./src/routes/tutoring')
}

const app = express();

app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, 'dist'), { extensions: ['html'] }));

app.get('*', (req, res) => {
  return res.redirect('/page_not_found')
})

app.post('/tutoring', routes.tutoring);

const server = app.listen(env.PORT);

module.exports = server;
