const async = require('async');
const chai = require('chai');
const request = require('supertest');
const { env } = require('process');
const mailgun = require('mailgun-js');
const spies = require('chai-spies')
chai.use(spies)
const should = chai.should()
const expect = chai.expect

const tutoring = require('./tutoring')

const page = '/tutoring';
const tutor = { email: 'foo@bar.com' }
const tutorie = { email: 'foo@bar.com' }
const subject = 'foobar'
const message = 'foo'

describe('tuturing API', function () {

  describe('POST /tutoring', function () {

    const sandbox = chai.spy.sandbox();
    let app;
    beforeEach(function () {
      app = require('../../app');
      sandbox.on(tutoring, ['sendTutorEmail'])
    });
    afterEach(function () {
      app.close();
      sandbox.restore()
    });

    describe('catch invalid format response', () => {
      const errMessage = 'Invalid Format'

      it('catches empty body', () => request(app).post(page).send({}).expect(200, { error: errMessage}))
      it('catches emptyTutor', () => request(app).post(page).send({tutor: {}, tutorie, subject, message}).expect(200, { error: errMessage}))
      it('catches emptyTutorie', () => request(app).post(page).send({tutor, tutorie: {}, subject, message}).expect(200, { error: errMessage}))
      it('catches emptySubject', () => request(app).post(page).send({tutor, tutorie: {}, subject: null, message}).expect(200, { error: errMessage}))
      it('catches emptyMessage', () => request(app).post(page).send({tutor, tutorie, subject, message: null}).expect(200, { error: errMessage}))

      it('catches justTutor', () => request(app).post(page).send({tutor}).expect(200, { error: errMessage}))
      it('catches justTutorie', () => request(app).post(page).send({tutorie}).expect(200, { error: errMessage}))
      it('catches justSubject', () => request(app).post(page).send({subject}).expect(200, { error: errMessage}))
      it('catches justMessage', () => request(app).post(page).send({message}).expect(200, { error: errMessage}))

      it('does not send email', () => expect(tutoring.sendTutorEmail).to.not.have.been.called());

    })

    describe('respond to valid request', () => {
      const sandbox = chai.spy.sandbox();
      beforeEach(function() {
        app = require('../../app');
      });

      afterEach(function() {
        app.close();
        sandbox.restore();
      });

      it('should return success response', done =>{
        request(app).post(page).send({tutor, tutorie, subject, message}).expect(200, {success: true})
          .then(() => {return done()})
      })
    })
  })
})
