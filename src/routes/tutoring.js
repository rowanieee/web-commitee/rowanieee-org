const mailgunfile = require('../../mailgunfile')
const mailgun = require('mailgun-js')(mailgunfile);
const tutoring = require('../resources/tutoring.json');

module.exports = async function(req, res) {
  if(req.method === 'POST') {
    const { tutor, tutorie, subject, message } = req.body;
    if(tutor && tutor.email && tutorie && tutorie.email && subject && message) {
      try{
        await sendTutorEmail(tutor, tutorie, subject, message);
        return res.json({success: true});
      } catch(err) {
        return res.json({error: err.message});
      }
    }
    else {
      const error = "Invalid Format"
      return res.json({error});
    }
  }
}

async function sendTutorEmail(tutor, tutorie, subject, message) {

  const data = {
    from: 'IEEE Tutoring <tutoring@rowanieee.org>',
    cc: tutorie.email,
    to: tutor.email,
    subject: `Rowan IEEE Tutoring Request: ${subject}`,
    text:
    `
    Rowan IEEE Tutoring Request: ${subject}

    ${message}

    -------------------------------------------------

    -- IEEE Tutoring Bot
    `
  };

  try {
    await mailgun.messages().send(data)
  }
  catch(err) {
    console.error(err)
    throw err
  }

}
