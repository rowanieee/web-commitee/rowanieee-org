# Contributing

In order to become a contributor to this respository, contact the current Rowan IEEE Webmaster, John McAvoy (@JOHNeMac36), to join the Rowan IEEE Web Committee.

Once you are added to this project, you should read our [wiki](https://gitlab.com/rowanieee/web-commitee/rowanieee-org/-/wikis/home) for further information about how to integrate with our workflow.

Author: John McAvoy (@JOHNeMac36)