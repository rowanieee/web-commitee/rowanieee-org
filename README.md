# rowanieee.org
Rowan IEEE Student Branch Website

## Source Code
See our [GitLab repository](https://gitlab.com/rowanieee/web-commitee/rowanieee-org)

## Wiki
See our [wiki](https://gitlab.com/rowanieee/web-commitee/rowanieee-org/-/wikis/home)

## Contributing
See [CONTRIBUTING.md](./CONTRIBUTING.md)

## Documentation
See [dev.rowanieee.org/docs](http://dev.rowanieee.org/docs)

## Coverage Report
See [dev.rowanieee.org/coverage](http://dev.rowanieee.org/coverage)

Author: John McAvoy (@JOHNeMac36)
