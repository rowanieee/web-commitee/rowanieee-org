const {env} = require('process')
const dotenv = require('dotenv');
dotenv.config();

module.exports = {
  apiKey: env.MAILGUN_API_KEY,
  domain: env.MAILGUN_DOMAIN
}
